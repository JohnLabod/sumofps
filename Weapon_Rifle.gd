extends Spatial

const DAMAGE = 4

var ammo_in_weapon = 10

const MAX_AMMO = 101
const RELOAD_TIME = 0.1
const IDLE_ANIM_NAME = "idle_rifle"
const FIRE_ANIM_NAME = "fire_rifle"

var bullet_scene = preload("bullet.tscn")
var is_weapon_enabled = false
var reload_timer = 0

var player_node = null

func _physics_process(delta):
	reload_timer -= delta

func fire_weapon():
	var clone = bullet_scene.instance()
	var scene_root = get_tree().root.get_children()[0]
	scene_root.add_child(clone)
	ammo_in_weapon -= 1
	clone.global_transform = self.global_transform
	clone.scale = Vector3(2, 2, 2)
	clone.BULLET_DAMAGE = DAMAGE
	clone.player_that_shot = player_node
	player_node.create_sound("Rifle_shot", self.global_transform.origin)

func equip_weapon():
	if player_node.current_weapon_name == null:
		player_node.animation_manager.set_animation("equip_rifle")

func add_ammo():
	if reload_timer > 0:
		pass
	else:
		ammo_in_weapon += 1
		reload_timer = RELOAD_TIME