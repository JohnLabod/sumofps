extends AnimationTree

var states = {
	"idle_unarmed":["equip_rifle", "idle_unarmed"],

	"equip_rifle":["idle_rifle"],
	"fire_rifle":["idle_rifle", "run_rifle"],
	"idle_rifle":["fire_rifle", "reload_rifle", "unequip_rifle", "idle_rifle", "run_rifle"],
	"reload_rifle":["idle_rifle"],
	"unequip_rifle":["idle_unarmed"],
	"run_rifle":["idle_rifle", "fire_rifle"],

	"run":["idle_unarmed"]
}

var animation_name_to_numbers = {"equip_rifle": 0, "fire_rifle": 1, "idle_rifle": 2, "run": 3, "run_rifle": 4}

var animation_speeds = {
	"idle_unarmed":1,
	"equip_rifle":2,
	"fire_rifle":6,
	"idle_rifle":1,
	"reload_rifle":1.45,
	"unequip_rifle":2,

}

var current_state = null

func _ready():
	pass #connect("animation_finished", $PlayerMesh/AnimationPlayer, "animation_ended")

func set_animation(animation_name):
	if animation_name == current_state:
		print ("AnimationPlayer_Manager.gd -- WARNING: animation is already ", animation_name)
		return true

	if current_state != null:
		var possible_animations = states[current_state]
		if animation_name in possible_animations:
			current_state = animation_name
			self["parameters/state/current"] = animation_name_to_numbers[animation_name]
			return true
		else:
			print ("AnimationPlayer_Manager.gd -- WARNING: Cannot change to ", animation_name, " from ", current_state)
			return false
	else:
		current_state = animation_name
		self["parameters/state/current"] = animation_name_to_numbers[animation_name]
		return true
	return false

func animation_ended(anim_name):
	print(anim_name)
	# UNARMED transitions
	if current_state == "idle_unarmed":
		pass
	# RIFLE transitions
	elif current_state == "equip_rifle":
		set_animation("idle_rifle")
	elif current_state == "fire_rifle":
		set_animation("idle_rifle")














