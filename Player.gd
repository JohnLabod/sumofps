extends KinematicBody

var player_number = 0

const GRAVITY = -35.8
const MAX_SPEED = 35
const JUMP_SPEED = 25
const ACCEL = 4.5
const DEACCEL = 16
const MAX_SLOP_ANGLE = 40
const MAX_HEALTH = 150
const MOUSE_SENSITIVITY = 0.1

var simple_audio_player = preload("res://Simple_Audio_Player.tscn")
var PlayerActions = preload("res://PlayerActions.gd")

var player_actions
var dir = Vector3()
var vel = Vector3()

var camera
var rotation_helper
var animation_manager
var feet_animations
var reload_spot

var weapons = {"RIFLE":null}
var current_weapon_name = null
var changing_weapon_name = null

var is_reloading = false
var health = 100
var bullet_boost = 10

var spine
var skeleton

var UI_status_label

func _ready():
	camera = $Rotation_Helper/Camera
	rotation_helper = $Rotation_Helper
	reload_spot = $ReloadSpot
	
	#get spine for rotation
	skeleton = $PlayerMesh/metarig/Skeleton
	spine = skeleton.find_bone("spine.003")
	
	animation_manager = $PlayerMesh/AnimationPlayer
	feet_animations = $PlayerMesh/FeetAnimations
	
	weapons["RIFLE"] = $Rotation_Helper/Gun_Fire_Points/Rifle_Point
	
	var gun_aim_point_pos = $Rotation_Helper/Gun_Aim_Point.global_transform.origin
	
	for weapon in weapons:
		var weapon_node = weapons[weapon]
		if weapon_node != null:
			weapon_node.player_node = self
			#weapon_node.look_at(gun_aim_point_pos, Vector3(0, 1, 0))
			#weapon_node.rotate_object_local(Vector3(0, 1, 0), deg2rad(180))

	changing_weapon_name = "RIFLE"

	#UI_status_label = $Rotation_Helper/Camera/HUD/Panel/Gun_label
	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	player_actions = PlayerActions.new()
	player_actions.set_actions(self, player_number)
	
	var spawns = get_node("/root/Stage/SpawnPoints")
	var spawn = spawns.get_children()[player_number]
	if spawn != null:
			var position = spawn.global_transform.origin
			global_transform.origin = position

func _physics_process(delta):
	player_actions.process_input(delta)
	if is_reloading == false:
		process_movement(delta)
		process_change_weapons(delta)
	#process_UI(delta)

func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()
	
	vel.y += delta * GRAVITY
	
	var hvel = vel
	hvel.y = 0
	
	var target = dir
	target *= MAX_SPEED
	
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL
	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel, Vector3(0, 1, 0), 0.05, 4, deg2rad(MAX_SLOP_ANGLE))
	#running animations
	if(is_on_floor()):
		if(vel.z == 0):
			if(feet_animations.current_animation != "stand"):
				feet_animations.stop()
				feet_animations.play("stand")
		elif(vel.z > 0):
			feet_animations.play("run")
		elif(vel.z < 0):
			feet_animations.play_backwards("run")

func _input(event):
	player_actions.move_camera(event)

func process_change_weapons(delta):
	if current_weapon_name != changing_weapon_name:
		weapons[changing_weapon_name].equip_weapon()
		current_weapon_name = changing_weapon_name

func fire_bullet():
	if current_weapon_name != null:
		weapons[current_weapon_name].fire_weapon()
		var blowback = weapons[current_weapon_name].DAMAGE / -2
		var direction_vect = global_transform.basis.z.normalized() * bullet_boost
		vel += direction_vect * blowback

func reload():
	player_actions.rotate_player(Vector2(0.0, 20.0), MOUSE_SENSITIVITY)
	var camera_rot = rotation_helper.rotation_degrees
	if camera_rot.x > 54:
		weapons[current_weapon_name].add_ammo()

func process_UI(delta):
	if current_weapon_name != null:
		var current_weapon = weapons[current_weapon_name]
		UI_status_label.text = "HEALTH: " + str(health) + \
			"\nAMMO: " + str(current_weapon.ammo_in_weapon)

func create_sound(sound_name, position=null):
	var audio_clone = simple_audio_player.instance()
	var scene_root = get_tree().root.get_children()[0]
	scene_root.add_child(audio_clone)
	audio_clone.play_sound(sound_name, position)

func add_health(additional_health):
	health += additional_health
	health = clamp(health, 0, MAX_HEALTH)

func bullet_hit(damage, bullet_global_trans, player_that_shot):
	if player_that_shot != self:
		bullet_boost += damage
		var direction_vect = bullet_global_trans.basis.z.normalized() * bullet_boost
		vel = direction_vect * damage








func _on_AnimationPlayer_animation_finished(anim_name):
	pass # Replace with function body.
