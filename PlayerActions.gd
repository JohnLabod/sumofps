extends Node

const action = [
["movement_forward_player_one", "movement_backward_player_one", "movement_left_player_one", "movement_right_player_one", "movement_jump_player_one", "fire_player_one", "reload_player_one", "menu_player_one"],
["movement_forward_player_two", "movement_backward_player_two", "movement_left_player_two", "movement_right_player_two", "movement_jump_player_two", "fire_player_two", "reload_player_two", "menu_player_two"],
]

enum {IDLE, RUN, JUMP}
var player
var state
var anim
var new_anim


var forward
var backward
var left
var right
var jump
var fire
var reload
var menu

func set_actions(player_obj, player_number):
	forward = action[player_number][0]
	backward = action[player_number][1]
	left = action[player_number][2]
	right = action[player_number][3]
	jump = action[player_number][4]
	fire = action[player_number][5]
	reload = action[player_number][6]
	menu = action[player_number][7]
	
	player = player_obj

func process_input(delta):
	
	player.dir = Vector3()
	var cam_xform = player.camera.get_global_transform()
	
	
	if Input.is_action_pressed(fire):
		if player.current_weapon_name != null:
			var current_weapon = player.weapons[player.current_weapon_name]
			if current_weapon.ammo_in_weapon > 0:
				if player.animation_manager.current_state == current_weapon.IDLE_ANIM_NAME:
					player.animation_manager.set_animation(current_weapon.FIRE_ANIM_NAME)
	var input_movement_vector = Vector2()
	if Input.is_action_pressed(forward):
		input_movement_vector.y += 1
	if Input.is_action_pressed(backward):
		input_movement_vector.y -= 1
	if Input.is_action_pressed(left):
		input_movement_vector.x -= 1
	if Input.is_action_pressed(right):
		input_movement_vector.x += 1
	
	input_movement_vector = input_movement_vector.normalized()
	
	player.dir += -cam_xform.basis.z.normalized() * input_movement_vector.y
	player.dir += cam_xform.basis.x.normalized() * input_movement_vector.x
	
	if player.is_on_floor():
		if Input.is_action_just_pressed(jump):
			player.vel.y = player.JUMP_SPEED
			player.feet_animations.play("jump")
		if Input.is_action_pressed(reload):
			if player.current_weapon_name != null:
				var current_weapon = player.weapons[player.current_weapon_name]
				if current_weapon.ammo_in_weapon < current_weapon.MAX_AMMO:
					player.is_reloading = true
					player.reload()
		else:
			player.is_reloading = false
	if Input.is_action_just_pressed(menu):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func move_camera(event):
	if player.is_reloading == false:
		if player.player_number == 0:
			if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
				rotate_player(event.relative, player.MOUSE_SENSITIVITY)


func rotate_player(rotation_amount, sensitivity):
	#rotate camera
	player.rotation_helper.rotate_x(deg2rad(rotation_amount.y * sensitivity))
	player.rotate_y(deg2rad(rotation_amount.x * sensitivity * -1))
	var camera_rot = player.rotation_helper.rotation_degrees
	camera_rot.x = clamp(camera_rot.x, -65, 55)
	player.rotation_helper.rotation_degrees = camera_rot
	
	#rotate spine
	if(camera_rot.x > -65 and camera_rot.x < 55):
		var spine_transform = player.skeleton.get_bone_pose(player.spine)
		spine_transform.basis = player.rotation_helper.transform.basis
		spine_transform.orthonormalized()
		player.skeleton.set_bone_pose(player.spine, spine_transform)